'use strict';

import fs from 'fs';
import path from 'path';
import gulp from 'gulp';
import del from 'del';
import runSequence from 'run-sequence';
import browserSync from 'browser-sync';
import gulpLoadPlugins from 'gulp-load-plugins';
import {output as pagespeed} from 'psi';
import pkg from './package.json';

import * as critical from 'critical';

const $ = gulpLoadPlugins();
const reload = browserSync.reload;

let minifyCode = true;

// Lint JavaScript
gulp.task('lint', () =>
  gulp.src('src/scripts/**/*.js')
    .pipe($.eslint())
    .pipe($.eslint.format())
    .pipe($.if(!browserSync.active, $.eslint.failOnError()))
);

// Optimize images
gulp.task('images', () =>
  gulp.src('src/images/**/*')
    .pipe($.cache($.imagemin({
      progressive: true,
      interlaced: true
    })))
    .pipe(gulp.dest('dist/images'))
    .pipe($.size({title: 'images'}))
);

// Copy all files at the root level (src)
gulp.task('copy', ['copy-vendor'], () =>
  gulp.src([
    'src/!(_)*',
    '!src/*.html'
  ], {
    dot: true
  }).pipe(gulp.dest('dist'))
    .pipe($.size({title: 'copy'}))
);

// Copy vendor files (bower_components)
gulp.task('copy-vendor', () =>
  gulp.src([
    'src/bower_components/**/*.js',
    'src/bower_components/**/*.css'
  ], {
    dot: true
  }).pipe(gulp.dest('dist/bower_components'))
    .pipe($.size({title: 'copy-vendor'}))
);

// Compile and automatically prefix stylesheets
gulp.task('styles', () => {
  const AUTOPREFIXER_BROWSERS = [
    'ie >= 10',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4.4',
    'bb >= 10'
  ];

  return gulp.src([
    'src/styles/**/*.scss',
    'src/styles/**/*.css'
  ])
    .pipe($.newer('.tmp/styles'))
    .pipe($.sourcemaps.init())
    .pipe($.sass({
      precision: 10,
      outputStyle: 'expanded',
    }).on('error', $.sass.logError))
    .pipe($.autoprefixer(AUTOPREFIXER_BROWSERS))
    .pipe(gulp.dest('.tmp/styles'))
    .pipe($.if(minifyCode, $.uncss({
      ignore: [/\.open>.dropdown-toggle/, /\.fade/, /\.collaps/, /\.js/, /\.is/],
      html: ['.tmp/*.html']
    })))
    // Concatenate and minify styles
    .pipe($.if(minifyCode && '*.css', $.cssnano()))
    .pipe($.size({title: 'styles'}))
    .pipe($.sourcemaps.write('./'))
    .pipe(gulp.dest('dist/styles'));
});

// Concatenate and minify JavaScript
gulp.task('scripts', () =>
    gulp.src('.tmp/scripts/app.min.js')
      .pipe($.newer('.tmp/scripts'))
      .pipe($.sourcemaps.init())
      .pipe($.babel())
      .pipe($.sourcemaps.write())
      .pipe(gulp.dest('dist/scripts'))
      //.pipe($.concat('app.js'))
      .pipe($.if(minifyCode, $.uglify({preserveComments: 'some'})))
      // Output files
      .pipe($.size({title: 'scripts'}))
      .pipe($.sourcemaps.write('.'))
      .pipe(gulp.dest('dist/scripts'))
);

// Scan your HTML for assets & optimize them
gulp.task('html', ['nunjucks'], () => {
  return gulp.src('.tmp/**/*.html')
    .pipe($.useref({
      searchPath: '{.tmp,src}',
      noAssets: true
    }))

    // Minify any HTML...
    .pipe($.if(minifyCode && '*.html', $.htmlmin({
      removeComments: true,
      collapseWhitespace: true,
      collapseBooleanAttributes: true,
      minifyCSS: true,
      minifyJS: true,
      processScripts: 'application/ld+json',
      removeAttributeQuotes: true,
      removeRedundantAttributes: true,
      removeEmptyAttributes: true,
      removeScriptTypeAttributes: true,
      removeStyleLinkTypeAttributes: true,
      //removeOptionalTags: true
    })))
    // or prettify HTML for unminified build
    .pipe($.if(!minifyCode && '*.html', $.prettify({
      unformatted: ['pre', 'code', 'script'],
      indent_size: 2,
      preserve_newlines: true,
      end_with_newline: true,
    })))
    // Output files
    .pipe($.if('*.html', $.size({title: 'html', showFiles: true})))
    .pipe(gulp.dest('dist'));
});

// Compile nunjucks templates
gulp.task('nunjucks', ['json'], () => {
  return gulp.src([
    // Exclude folders starting with underscore from compiling
    '!src/_*/**',
    '!src/bower_components/**',
    'src/**/*.html',
  ])
    .pipe($.data(file => {
      // Read combined.json file
      try {
        return JSON.parse(fs.readFileSync('src/_data/combined.json'));
      } catch (err) {
        if (err.code === 'ENOENT') {
          // If file combined.json is not found, do nothing
        } else {
          console.error(err);
        }
      }
    }))
    .pipe($.nunjucksApi({
      autoescape: false,
      src: 'src',
      fs: fs
    }))
    .pipe(gulp.dest('.tmp'));
});

// Combine multiple JSON files
gulp.task('json', () => {
  return gulp.src('src/_data/**/!(combined)*.json')
    .pipe($.jsoncombine('combined.json', (data) => {
      return new Buffer(JSON.stringify(data));
    }))
    .pipe(gulp.dest('src/_data/'));
});

// Clean output directory
gulp.task('clean', () => del(['.tmp', 'dist/*', '!dist/.git'], {dot: true}));

// Watch files for changes & reload
gulp.task('serve', ['html', 'scripts', 'styles'], () => {
  browserSync({
    notify: false,
    // Run as an https by uncommenting 'https: true'
    // https: true,
    server: ['.tmp', 'src'],
    port: 3000
  });

  gulp.watch(['src/**/*.{html,json}'], ['nunjucks', reload]);
  gulp.watch(['src/styles/**/*.{scss,css}'], ['styles', reload]);
  gulp.watch(['src/scripts/**/*.js'], ['lint', 'scripts', reload]);
  gulp.watch(['src/images/**/*'], reload);
});

// Build and serve the output from the dist build
gulp.task('serve:dist', ['default'], () =>
  browserSync({
    notify: false,
    // Run as an https by uncommenting 'https: true'
    // https: true,
    server: 'dist',
    port: 3001
  })
);

// Build production files, the default task
gulp.task('default', ['clean'], cb =>
  runSequence(
    'html', 'styles',
    ['lint', 'scripts', 'images', 'copy'],
    cb
  )
);

// Generate & Inline Critical-path CSS
gulp.task('critical', () => {
  return gulp.src('dist/*.html')
    .pipe(critical.stream({
      base: 'dist/',
      inline: true,
      css: ['dist/styles/app.css','dist/styles/bootstrap.css'],
      dimensions: [{
        width: 360,
        height: 640
      }, {
        width: 1366,
        height: 768
      }],
      minify: true,
    }))
    .on('error', function(error) {
      console.log(error.message);
    })
    .pipe(gulp.dest('dist/'));
});

// Build unminified files, for demo purposes
gulp.task('build:unmin', ['clean'], cb => {
  minifyCode = false,
  runSequence(
    'styles',
    ['lint', 'html', 'scripts', 'images', 'copy'],
    cb
  )
});

// Run PageSpeed Insights
gulp.task('pagespeed', cb =>
  // Update the below URL to the public URL of your site
  pagespeed('https://genesisui.com/demo/real/bootstrap4-angular2/#/dashboard', {
    strategy: 'mobile'
  }, cb)
);
