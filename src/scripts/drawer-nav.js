import { forEach } from './util';

/**
 * Expand / collapse nav-groups
 */
export function toggleNav(event) {
  event.preventDefault();
  var srcElement = event.currentTarget;
  var srcParent = srcElement.parentElement;
  // Grabs the nav-toggle and the nav-group so we can update them
  var toggle = srcParent.querySelector('.nav-toggle');
  var group = srcParent.querySelector('.nav-group');
  // Toggles the expanded/collapsed classes
  toggle.classList.toggle('nav-toggle-expanded');
  toggle.classList.toggle('nav-toggle-collapsed');
  group.classList.toggle('nav-group-collapsed');
  group.classList.toggle('nav-group-expanded');
}

/**
 * Add event listeners for each drawer nav-toggle
 */
export function initNavToggles() {
  var navToggles = document.querySelectorAll('.nav-toggle');
  forEach(navToggles, function(idx) {
    navToggles[idx].addEventListener('click', toggleNav);
  });
}