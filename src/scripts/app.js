import * as drawer from './drawer';
import * as drawerNav from './drawer-nav';

drawer.initDrawerToggles();
drawer.init();
drawer.drawerToggle();

drawerNav.initNavToggles();
