import { forEach } from './util';

const activeClass = 'open';
const transitioningClass = 'is-transitioning';
const gridFloatBreakpoint = 991;

/**
 * Add event listeners to drawer toggles
 */
export function initDrawerToggles() {
  var drawerToggles = document.querySelectorAll('[data-toggle="drawer"]');
  forEach(drawerToggles, function(idx) {
    drawerToggles[idx].addEventListener('click', toggleDrawer);
  })
}

/**
 * Open on correct sizes
 */
export function toggleDrawerVisibility(element) {
  var elementID = element.id;
  var wasClosed = localStorage.getItem(elementID + '-open') === 'false';

  if (wasClosed) {
    element.classList.remove(activeClass);
  } else if (window.innerWidth >= gridFloatBreakpoint) {
    element.classList.add(activeClass);
  } else {
    element.classList.remove(activeClass);
  }
}

/**
 * Click handler
 */
export function toggleDrawer(event) {
  event.preventDefault();
  var targetDrawer = event.currentTarget.getAttribute('data-target');
  var drawer = document.querySelector(targetDrawer);
  var drawerID = drawer.id;
/*
  if (window.innerWidth < gridFloatBreakpoint) {
    var drawers = document.querySelectorAll('.drawer');
    forEach(drawers, function(idx) {
      drawers[idx].classList.remove(activeClass);
    });
  }
*/
  drawer.classList.add(transitioningClass);
  drawer.classList.toggle(activeClass);

  drawer.addEventListener('transitionend', function() {
    this.classList.remove(transitioningClass);
  }, false);

  if (window.innerWidth >= gridFloatBreakpoint) {
    localStorage.setItem(drawerID + '-open', drawer.classList.contains(activeClass));
  }
}

export function closeDrawer(event) {
  var drawer = event.target.parentNode;
  drawer.classList.toggle(activeClass);
  drawer.classList.add(transitioningClass);
  setTimeout(function() {      
    drawer.classList.remove(transitioningClass);
  }, 300);
}

export function init() {
  var scrims = document.querySelectorAll('.scrim');
  forEach(scrims, function(idx) {
    scrims[idx].addEventListener('click', closeDrawer);
  });
}

export function drawerToggle() {
  var drawers = document.querySelectorAll('.drawer');
  forEach(drawers, function(idx) {
    toggleDrawerVisibility(drawers[idx]);
    window.addEventListener('resize', function() {
      toggleDrawerVisibility(drawers[idx])
    });
  });
}
